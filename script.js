// const map = [

//     "WWWWWWWWWWWWWWWWWWWWW",
//     "W   W     W     W W W",
//     "W W W WWW WWWWW W W W",
//     "W W W   W     W W   W",
//     "W WWWWWWW W WWW W W W",
//     "W         W     W W W",
//     "W WWW WWWWW WWWWW W W",
//     "W W   W   W W     W W",
//     "W WWWWW W W W WWW W F",
//     "S     W W W W W W WWW",
//     "WWWWW W W W W W W W W",
//     "W     W W W   W W W W",
//     "W WWWWWWW WWWWW W W W",
//     "W       W       W   W",
//     "WWWWWWWWWWWWWWWWWWWWW",
// ];

// I am going to convert the map above from gaps and "W"s to digits in order to help me organize this in a way I can 
// more easily convert to a maze on the DOM using the functions below. 

const mazeArray = [
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1],
    [1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1],
    [1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1],
    [1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1],
    [1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1],
    [1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1],
    [1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 3],
    [2, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1],
    [1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
    [1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1],
    [1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
]

function createMaze(grid) {
    for (i = 0; i < mazeArray.length; i++) {
        createRow();
        for (j = 0; j < mazeArray[i].length; j++) {
            if (mazeArray[i][j] == 1) {
                createWallCell();
            } else if (mazeArray[i][j] == 0) {
                createFloorCell();
            } else if (mazeArray[i][j] == 2) {
                createFloorCell();
            } else if (mazeArray[i][j] == 3) {
                createEnd();
            }

        }
    }
}
createMaze(mazeArray);

function createRow() {
    let row = document.createElement("div");
    row.className = "row";
    row.id = "row" + i;
    let parentDiv = document.getElementById("box");
    parentDiv.appendChild(row);
}


function createWallCell() {
    let wallCell = document.createElement("div");
    wallCell.className = "wall";
    wallCell.id = "cell" + i + "-" + j;
    let parentDiv = document.getElementById("row" + i);
    parentDiv.appendChild(wallCell);
}

function createFloorCell() {
    let floorCell = document.createElement("div");
    floorCell.className = "floor";
    floorCell.id = "cell" + i + "-" + j;
    let parentDiv = document.getElementById("row" + i);
    parentDiv.appendChild(floorCell);
}

function createEnd() {
    let end = document.createElement("div");
    end.className = "floor";
    end.id = "cell" + i + "-" + j;
    let parentDiv = document.getElementById("row" + i);
    parentDiv.appendChild(end);
}

function placeLegolas() {
    let legolas = document.createElement("div");
    legolas.className = "legolas";
    legolas.id = "legolas";
    let placeCell = document.getElementById("cell9-0");
    placeCell.appendChild(legolas);
}
placeLegolas();

document.addEventListener("keydown", moveLegolas);

function moveLegolas(e) {

    if (e.code == "ArrowRight") {
        moveRight();
    } else if (e.code == "ArrowLeft") {
        moveLeft();
    } else if (e.code == "ArrowUp") {
        moveUp();
    } else if (e.code == "ArrowDown") {
        moveDown();
    }

    checkForWin();
}

let playerRow = 9;
let playerCell = 0;

// player starts at 9-0; when we run the move function we do 1. check to make sure the move isn't out of bounds,
// 2. check to make sure it's not a wall, 3. move, 4. update the position 

function moveRight() {
    if (document.getElementById("cell" + playerRow + "-" + (playerCell + 1)).className === "floor") {
        let newPosition = document.getElementById("cell" + playerRow + "-" + (playerCell + 1));
        newPosition.appendChild(legolas);
        playerCell += 1
    } else if (document.getElementById("cell" + playerRow + "-" + (playerCell + 1)).className !== "floor") {
        return
    }
}

function moveLeft() {
    if (document.getElementById("cell" + playerRow + "-" + (playerCell - 1)).className === "floor") {
        let newPosition = document.getElementById("cell" + playerRow + "-" + (playerCell - 1));
        newPosition.appendChild(legolas);
        playerCell -= 1
    } else if (document.getElementById("cell" + playerRow + "-" + (playerCell - 1)).className !== "floor") {
        return
    }
}

function moveUp() {
    if (document.getElementById("cell" + (playerRow - 1) + "-" + playerCell).className === "floor") {
        let newPosition = document.getElementById("cell" + (playerRow - 1) + "-" + playerCell);
        newPosition.appendChild(legolas);
        playerRow -= 1
    } else if (document.getElementById("cell" + (playerRow - 1) + "-" + playerCell).className !== "floor") {
        return
    }
}

function moveDown() {
    if (document.getElementById("cell" + (playerRow + 1) + "-" + playerCell).className === "floor") {
        let newPosition = document.getElementById("cell" + (playerRow + 1) + "-" + playerCell);
        newPosition.appendChild(legolas);
        playerRow += 1
    } else if (document.getElementById("cell" + (playerRow + 1) + "-" + playerCell).className !== "floor") {
        return
    }
}

function checkForWin() {
    if (document.getElementById("cell8-20").childElementCount === 1) {
        alert("YOU WIN! Reload the page to play again")
    }
}